import React, { Component } from 'react'
import { Container } from "../components/Container";
import { ThemeProvider } from "styled-components";
import { ToDoListDarkTheme } from "../themes/ToDoListDarkTheme";
import { ToDoListLightTheme } from "../themes/ToDoListLightTheme";
import { ToDoListPrimaryTheme } from "../themes/ToDoListPrimaryTheme";
import { Dropdown } from "../components/Dropdown";
import { Heading1, Heading3 } from "../components/Heading";
import { TextField } from "../components/TextField";
import { Button } from "../components/Button";
import { Table, Th, Thead, Tr } from "../components/Table";
export default class ToDoList extends Component {
    render() {
        return (
            <div>
                <ThemeProvider theme={ToDoListDarkTheme}>
                    <Container style={{ width: '50%' }}>
                        <Dropdown>
                            <option value="">Dark Theme</option>
                            <option value="">Light Theme</option>
                            <option value="">Primary Theme</option>
                        </Dropdown>
                        <Heading3 className="display-4">TO DO LIST</Heading3>
                        <TextField label='Task name: ' style={{ width: "60%" }} ></TextField>
                        <Button className='ml-2'><i class="fa fa-plus"></i> ADD TASK</Button>
                        <Button className='ml-2'><i class="fa fa-upload"></i> UPDATE TASK</Button>
                        <hr />
                        <Heading3> Task to do</Heading3>
                        <Table>
                            <Thead>
                                <Tr>
                                    <Th className='text-left' style={{ verticalAlign: "middle" }}>Task</Th>
                                    <Th className='text-right'>
                                        <Button className='ml-2'><i class="fa fa-edit"></i></Button>
                                        <Button className='ml-2'><i class="fa fa-check"></i></Button>
                                        <Button className='ml-2'><i class="fa fa-trash"></i></Button>

                                    </Th>
                                </Tr>
                                <Tr>
                                    <Th className='text-left' style={{ verticalAlign: "middle" }}>Task</Th>
                                    <Th className='text-right'>
                                        <Button className='ml-2'><i class="fa fa-edit"></i></Button>
                                        <Button className='ml-2'><i class="fa fa-check"></i></Button>
                                        <Button className='ml-2'><i class="fa fa-trash"></i></Button>

                                    </Th>
                                </Tr>
                            </Thead>
                        </Table>
                        <hr />
                        <Heading3> Task completed</Heading3>
                        <Table>
                            <Thead>
                                <Tr>
                                    <Th className='text-left' style={{ verticalAlign: "middle" }}>Task</Th>
                                    <Th className='text-right'>

                                        <Button className='ml-2'><i class="fa fa-trash"></i></Button>

                                    </Th>
                                </Tr>
                                <Tr>
                                    <Th className='text-left' style={{ verticalAlign: "middle" }}>Task</Th>
                                    <Th className='text-right'>

                                        <Button className='ml-2'><i class="fa fa-trash"></i></Button>

                                    </Th>
                                </Tr>
                            </Thead>
                        </Table>
                    </Container>
                </ThemeProvider>

            </div >
        )
    }
}
